package com.codecool.dpmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DpmapApplication {

    public static void main(String[] args) {
        SpringApplication.run(DpmapApplication.class, args);
    }

}
